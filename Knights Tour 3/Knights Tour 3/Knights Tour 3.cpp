#include <iostream>
#include <iomanip>
#include "stdafx.h"
using namespace std;

int counter = 1;
int **arrptr;

class horse
{
public:
	horse(int);
	bool backtrack(int, int);
	void print();
private:
	int size;
	void mark(int &);
	void unmark(int &);
	bool unvisited(int &);
};

horse::horse(int s)
{
	int i, j;
	size = s;
	for (i = 0; i <= s - 1; i++){
		for (j = 0; j <= s - 1; j++){
			arrptr[i][j] = 0;
		}
	}
}

void horse::mark(int &val)
{
	val = counter;
	counter++;
}

void horse::unmark(int &val)
{
	val = 0;
	counter--;
}

void horse::print()
{
	
	cout << "\n - - - - - - - - - - - - - - - - - -\n";
	for (int i = 0; i <= size - 1; i++){
		cout << "| ";
		for (int j = 0; j <= size - 1; j++)
			cout << setw(2) << setfill('0') << arrptr[i][j] << " | ";
		cout << "\n - - - - - - - - - - - - - - - - - -\n";
	}
	
}

bool horse::backtrack(int x, int y)
{

	if (counter > (size * size))
		return true;

	if (unvisited(arrptr[x][y])){
		if ((x - 2 >= 0) && (y + 1 <= (size - 1)))
		{
			mark(arrptr[x][y]);
			if (backtrack(x - 2, y + 1))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x - 2 >= 0) && (y - 1 >= 0))
		{
			mark(arrptr[x][y]);
			if (backtrack(x - 2, y - 1))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x - 1 >= 0) && (y + 2 <= (size - 1)))
		{
			mark(arrptr[x][y]);
			if (backtrack(x - 1, y + 2))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x - 1 >= 0) && (y - 2 >= 0))
		{
			mark(arrptr[x][y]);
			if (backtrack(x - 1, y - 2))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x + 2 <= (size - 1)) && (y + 1 <= (size - 1)))
		{
			mark(arrptr[x][y]);
			if (backtrack(x + 2, y + 1))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x + 2 <= (size - 1)) && (y - 1 >= 0))
		{
			mark(arrptr[x][y]);
			if (backtrack(x + 2, y - 1))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x + 1 <= (size - 1)) && (y + 2 <= (size - 1)))
		{
			mark(arrptr[x][y]);
			if (backtrack(x + 1, y + 2))
				return true;
			else
				unmark(arrptr[x][y]);
		}
		if ((x + 1 <= (size - 1)) && (y - 2 >= 0))
		{
			mark(arrptr[x][y]);
			if (backtrack(x + 1, y - 2))
				return true;
			else
				unmark(arrptr[x][y]);
		}


	}
	return false;
}

bool horse::unvisited(int &val)
{
	if (val == 0)
		return 1;
	else
		return 0;
}

class initiation
{

public:initiation(int userBoardSize, int userStartPos)
{

		   counter = 1;
		   arrptr = new int*[userBoardSize];

		   for (int i = 0; i < userBoardSize; i++)
		   {
			   arrptr[i] = new int[userBoardSize];	
		   }

		   horse example((userBoardSize));

		   if (example.backtrack(userStartPos, userStartPos))
		   {
			   cout << " >>> Successful! <<< " << endl;
			   example.print();
			   printf("Solution found on %d tiles \n\n", ((userBoardSize*userBoardSize)));
			   printf("The numbers indicate each step in the Knights Tour (01 is starting position) \n\n");
		   }
		   else
		   {
			   cout << " >>> Not possible! <<< " << endl;
		   }
}

};

int main()
{
	int userBoardSize;
	int userStartPos;
	bool allowed = false;
	char j = 'y';
	while (j == 'y')
	{
		while (allowed == false)
		{
			printf("Please input board size (minimum is 5, 8 and above will take a while) \n");
			cin >> userBoardSize;
			if (cin.fail())
			{
				cin.clear();
				cin.ignore();
				printf("\nPlease enter a valid number...\n");
			}
			else if (userBoardSize < 5)
			{
				printf("\nMinimum boardsize is 5, please enter a valid size...\n");
			}
			else if (userBoardSize > 9)
			{
				printf("\nPlease don't enter a size above 9, let's not roast your computer...\n");
			}
			else
			{
				allowed = true;
			}
		}
		allowed = false;
		while (allowed == false)
		{
			printf("\nPlease input starting position, input 1 coordinate (minimum is 0, maximum is %d)\n", ((userBoardSize - 1)));
			cin >> userStartPos;
			printf("\n");
			if (cin.fail())
			{
				cin.clear();
				cin.ignore();
				printf("Please enter a valid number...");
			}
			else if (userStartPos < 0)
			{
				printf("Please don't enter a number lower than 0...");
			}
			else if (userStartPos >= userBoardSize)
			{
				printf("Please don't enter a position above %d...", ((userBoardSize - 1)));
			}
			else
			{
				allowed = true;
			}
		}
			allowed = false;
		initiation startKnightsTour(userBoardSize, userStartPos);
		printf("Solution found, replay? (y/n) \n");
		cin >> j;
		printf("\n");
	}
}